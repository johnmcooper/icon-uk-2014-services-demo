package org.iconuk.servicesdemo.service;

import java.util.HashSet;
import java.util.Set;

import org.iconuk.servicesdemo.resources.CurrentUserDetailsResource;
import org.iconuk.servicesdemo.resources.UserPaulDetailsResource;

import com.ibm.commons.log.Log;
import com.ibm.commons.log.LogMgr;
import com.ibm.domino.das.service.RestService;


public class NabDetailsService extends RestService {
	
	public static final LogMgr NABDETAILS_SERVICE_LOGGER = Log.load("org.iconuk.servicesdemo.service.NabDetails");
	
    public static final String NABDETAILS = "nabdetails";
    public static final String NABDETAILS_SERVICE_PATH = SERVICE_PATH + "/" + NABDETAILS;
    
    public static final String CURRENTUSER = "me";
    public static final String CURRENTUSER_PATH = NABDETAILS + "/" + CURRENTUSER;

    public static final String USERPAUL = "paul";
    public static final String USERPAUL_PATH = NABDETAILS + "/" + USERPAUL;

	public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        NABDETAILS_SERVICE_LOGGER.getLogger().fine("Adding mail resources.");
        classes.add(CurrentUserDetailsResource.class);
        classes.add(UserPaulDetailsResource.class);
        return classes;
    }
}
