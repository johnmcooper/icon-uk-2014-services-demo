package org.iconuk.servicesdemo.resources;

import static org.iconuk.servicesdemo.service.NabDetailsService.USERPAUL_PATH;
import static org.iconuk.servicesdemo.service.NabDetailsService.NABDETAILS_SERVICE_LOGGER;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.iconuk.servicesdemo.service.NabDetailsService;
import org.iconuk.servicesdemo.util.General;
import org.iconuk.servicesdemo.util.NabUtil;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;

import com.ibm.commons.util.io.json.JsonGenerator;
import com.ibm.commons.util.io.json.JsonJavaFactory;
import com.ibm.commons.util.io.json.JsonGenerator.StringBuilderGenerator;
import com.ibm.domino.osgi.core.context.ContextInfo;

/**
 * Current Logged in users details resource class
 */
@Path(USERPAUL_PATH)
public class UserPaulDetailsResource {
    
	private static final String[] PERSON_DOC_FIELDS_TO_DISPLAY = {"FullName","InternetAddress","JobTitle"};

	/* grab the current logged in users details and return some information about them */  
    @GET
    public Response getCurrentUserDetails() {
        NABDETAILS_SERVICE_LOGGER.traceEntry(this, "getCurrentUserDetails");
        
    	HashMap<String, Object> fieldValuesMap = new HashMap<String, Object>();
    	String jsonEntity = null;
        
        //check the current user is authenticated
		Session session = ContextInfo.getUserSession();
		try {
			if("anonymous".equalsIgnoreCase(session.getEffectiveUserName())){
				throw new WebApplicationException(NabDetailsService.createErrorResponse("User must be authenticated", Response.Status.FORBIDDEN));
			}
		} catch (NotesException e1) {
			throw new WebApplicationException(NabDetailsService.createErrorResponse("Unable to grab current logged in user", Response.Status.FORBIDDEN));
		}
		
		Database dbNab = null;
		Document docUserPaul = null;
		try{
			// grab the current users person document from the nab
			dbNab = NabUtil.getNab();
			docUserPaul = NabUtil.getPerson(dbNab, "Paul Withers/JMC");
			
			//loop through list of fields to display, grab data from document and add to local hashmap
			for(String fieldName : PERSON_DOC_FIELDS_TO_DISPLAY){
				fieldValuesMap.put(fieldName, docUserPaul.getItemValueString(fieldName));
			}
			
		}catch(NotesException ne){
			throw new WebApplicationException(NabDetailsService.createErrorResponse(ne, Response.Status.INTERNAL_SERVER_ERROR));
		}finally{
			General.incinerate(docUserPaul, dbNab);
		}

    	// Serialize the JSON from the hashmap
        try {
            StringBuilder sb = new StringBuilder();
            JsonGenerator.Generator generator = new StringBuilderGenerator(JsonJavaFactory.instanceEx, sb, false);
            generator.toJson(fieldValuesMap);
            jsonEntity = sb.toString();
        } catch (Exception e) {
            throw new WebApplicationException(NabDetailsService.createErrorResponse(e, Response.Status.INTERNAL_SERVER_ERROR));
        }
        
        // create the response
        ResponseBuilder builder = Response.ok();        
        if ( jsonEntity != null ) {
            builder.type(MediaType.APPLICATION_JSON).entity(jsonEntity);
        }
        
        Response response = builder.build();
        NABDETAILS_SERVICE_LOGGER.traceExit(this, "getCurrentUserDetails", response);

        return response;
    }
}
