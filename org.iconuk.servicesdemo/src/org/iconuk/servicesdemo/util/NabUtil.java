package org.iconuk.servicesdemo.util;

import com.ibm.domino.osgi.core.context.ContextInfo;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public class NabUtil {
	public static final String ADDRESSBOOK_PATH = "names.nsf";
	public static final String USER_LOOKUP_VIEW = "$Users";
	
	public static Database getNab(){
		Session session = ContextInfo.getUserSession();
		Database dbNab = null;
		try {
			dbNab = session.getDatabase("", "names.nsf");
		} catch (NotesException e) {
			// log the error
			dbNab = null;
		}
		return dbNab;
	}
	
	public static Document getPerson(Database dbNab, String username){
		Document docPerson = null;
		View vwUsers = null;
		try{
			vwUsers = dbNab.getView(USER_LOOKUP_VIEW);
			docPerson = vwUsers.getDocumentByKey(username, true);
			
		}catch(NotesException ne){
			// log the error
		}finally{
			General.incinerate(vwUsers);
		}
		
		return docPerson;
	}
}