package org.iconuk.servicesdemo.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import lotus.domino.Base;

public class General {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void incinerate(final Object... args) {

		for (Object o : args) {
			if (o != null) {
				if (o instanceof lotus.domino.Base) {
					try {
						((Base) o).recycle();
					} catch (Throwable t) {
						// who cares?
					}
				} else if (o instanceof Map) {
					Set<Map.Entry> entries = ((Map) o).entrySet();
					for (Map.Entry<?, ?> entry : entries) {
						incinerate(entry.getKey(), entry.getValue());
					}
				} else if (o instanceof Collection) {
					Iterator i = ((Collection) o).iterator();
					while (i.hasNext()) {
						Object obj = i.next();
						incinerate(obj);
					}
				} else if (o.getClass().isArray()) {
					try {
						Object[] objs = (Object[]) o;
						for (Object ao : objs) {
							incinerate(ao);
						}
					} catch (Throwable t) {
						// who cares?
					}
				}
			}
		}
	}
}
